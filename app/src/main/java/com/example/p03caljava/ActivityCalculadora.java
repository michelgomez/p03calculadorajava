package com.example.p03caljava;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.p03caljava.databinding.ActivityCalculadoraBinding;

public class ActivityCalculadora extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtNum1;
    private EditText txtNum2;
    private TextView lblResultado;
    private Calculadora calculadora = new Calculadora(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicar();
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void limpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
            }
        });
        confirmar.show();
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnDividir = findViewById(R.id.btnDividir);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        lblResultado = findViewById(R.id.lblResultado);
    }

    private void btnSumar() {
        String num1Text = txtNum1.getText().toString();
        String num2Text = txtNum2.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));
            lblResultado.setText(String.valueOf(calculadora.suma()));
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnRestar() {
        String num1Text = txtNum1.getText().toString();
        String num2Text = txtNum2.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));
            lblResultado.setText(String.valueOf(calculadora.resta()));
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnMultiplicar() {
        String num1Text = txtNum1.getText().toString();
        String num2Text = txtNum2.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));
            lblResultado.setText(String.valueOf(calculadora.multiplicacion()));
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void btnDividir() {
        String num1Text = txtNum1.getText().toString();
        String num2Text = txtNum2.getText().toString();

        if (!num1Text.isEmpty() && !num2Text.isEmpty()) {
            calculadora.setNum1(Integer.parseInt(num1Text));
            calculadora.setNum2(Integer.parseInt(num2Text));
            lblResultado.setText(String.valueOf(calculadora.division()));
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }
}